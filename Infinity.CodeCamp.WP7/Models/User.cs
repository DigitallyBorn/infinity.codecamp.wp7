﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Infinity.CodeCamp.WP7.Framework;
using System.Net;
using System.Collections.ObjectModel;

namespace Infinity.CodeCamp.WP7.Models
{
    public delegate void ShowUserCallback(Exception exception, User user);

    public class User
    {
        [JsonProperty(PropertyName = "id")]
        public long id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "screen_name")]
        public string ScreenName { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }

        [JsonProperty(PropertyName = "profile_image_url")]
        public string ProfileImage { get; set; }

        [JsonProperty(PropertyName = "status")]
        public Tweet LastTweet { get; set; }

        [JsonProperty(PropertyName="url")]
        public string WebsiteUrl { get; set; }

        public Uri ProfileUri
        {
            get
            {
                return new Uri(string.Format("http://www.twitter.com/{0}", this.ScreenName), UriKind.Absolute);
            }
        }

        public static void GetUserDetails(long id, ShowUserCallback callback)
        {
            WebRequestState requestState = new WebRequestState();

            HttpWebRequest request = HttpWebRequest.CreateHttp(
                string.Format("http://api.twitter.com/1/users/show.json?user_id={0}", id));
            request.Method = "GET";

            requestState.Request = request;
            requestState.UserData.Add("Callback", callback);

            request.BeginGetResponse(GetUserDetailsEndRequest, requestState);
        }

        public static void GetUserDetailsEndRequest(IAsyncResult result)
        {
            WebRequestState requestState = (WebRequestState)result.AsyncState;
            ShowUserCallback callback = (ShowUserCallback)requestState.UserData["Callback"];

            try
            {
                WebResponse response = requestState.Request.EndGetResponse(result);

                string responseData = response.GetResponseStream().ReadString();

                User user = JsonConvert.DeserializeObject<User>(responseData);

                if (callback != null)
                    callback(null, user);
            }
            catch (Exception ex)
            {
                if (callback != null)
                    callback(ex, null);
            }
        }
    }
}
