﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using Infinity.CodeCamp.WP7.Framework;
using Newtonsoft.Json;

namespace Infinity.CodeCamp.WP7.Models
{
    public delegate void GetPublicTimelineCallback(Exception exception, ObservableCollection<Tweet> tweets);


    public class Tweet
    {
        #region Object Properties
        
        [JsonProperty(PropertyName = "id")]
        public long id { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "user")]
        public User User { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        [JsonConverter(typeof(TwitterDateConverter))]
        public DateTime DateCreated { get; set; }
        
        #endregion

        #region Get Public Timeline

        public static void GetPublicTimeline(GetPublicTimelineCallback callback)
        {
            WebRequestState requestState = new WebRequestState();

            HttpWebRequest request = HttpWebRequest.CreateHttp("http://api.twitter.com/1/statuses/public_timeline.json");
            request.Method = "GET";

            requestState.Request = request;
            requestState.UserData.Add("Callback", callback);

            request.BeginGetResponse(GetPublicTimelineEndRequest, requestState);
        }

        public static void GetPublicTimelineEndRequest(IAsyncResult result)
        {
            WebRequestState requestState = (WebRequestState)result.AsyncState;
            GetPublicTimelineCallback callback = (GetPublicTimelineCallback)requestState.UserData["Callback"];

            try
            {
                WebResponse response = requestState.Request.EndGetResponse(result);

                string responseData = response.GetResponseStream().ReadString();

                ObservableCollection<Tweet> tweets = JsonConvert.DeserializeObject<ObservableCollection<Tweet>>(responseData);

                if (callback != null)
                    callback(null, tweets);
            }
            catch (Exception ex)
            {
                if (callback != null)
                    callback(ex, null);
            }
        }

        #endregion

        #region Get User Timeline
        public static void GetUserTimeline(long id, GetPublicTimelineCallback callback)
        {
            WebRequestState requestState = new WebRequestState();

            HttpWebRequest request = HttpWebRequest.CreateHttp(
                string.Format("http://api.twitter.com/1/statuses/user_timeline.json?user_id={0}&trim_user=true&exclude_replies=true", id));
            request.Method = "GET";

            requestState.Request = request;
            requestState.UserData.Add("Callback", callback);

            request.BeginGetResponse(GetUserTimelineEndRequest, requestState);
        }

        public static void GetUserTimelineEndRequest(IAsyncResult result)
        {
            WebRequestState requestState = (WebRequestState)result.AsyncState;
            GetPublicTimelineCallback callback = (GetPublicTimelineCallback)requestState.UserData["Callback"];

            try
            {
                WebResponse response = requestState.Request.EndGetResponse(result);

                string responseData = response.GetResponseStream().ReadString();

                ObservableCollection<Tweet> tweets = JsonConvert.DeserializeObject<ObservableCollection<Tweet>>(responseData);

                if (callback != null)
                    callback(null, tweets);
            }
            catch (Exception ex)
            {
                if (callback != null)
                    callback(ex, null);
            }
        }
        #endregion

        public string TimeSinceCreation
        {
            get
            {
                TimeSpan timeSince = DateTime.Now.Subtract(this.DateCreated);

                if (timeSince.TotalSeconds < 1)
                    return "Less than 1 second ago";

                string timeSpan = string.Empty;
                int numberOfTimeItems = 0;

                if (timeSince.Hours > 0)
                {
                    numberOfTimeItems = timeSince.Hours;
                    timeSpan = "hour";
                }
                else if (timeSince.Minutes > 0)
                {
                    numberOfTimeItems = timeSince.Minutes;
                    timeSpan = "minute";
                }
                else if (timeSince.Seconds > 0)
                {
                    numberOfTimeItems = timeSince.Seconds;
                    timeSpan = "second";
                }

                return string.Format("{0} {1}{2} ago", numberOfTimeItems, timeSpan, numberOfTimeItems > 1 ? "s" : string.Empty);
            }
        }
    }
}
