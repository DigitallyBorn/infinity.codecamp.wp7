﻿using System;
using Infinity.CodeCamp.WP7.Framework;
using Infinity.CodeCamp.WP7.ViewModels;

namespace Infinity.CodeCamp.WP7.Views
{
    public partial class UserView : ViewPage
    {
        public UserView()
        {
            InitializeComponent();
            this.DataContext = new UserViewModel();
        }

        private void ViewApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            ((UserViewModel)this.DataContext).ViewUserOnTwitter();
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            ((UserViewModel)this.DataContext).ViewUserWebsite();

        }
    }
}