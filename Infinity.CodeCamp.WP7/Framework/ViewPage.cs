﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace Infinity.CodeCamp.WP7.Framework
{
    public class ViewPage: PhoneApplicationPage
	{
        protected ViewPage()
		{
			Loaded += PageBaseLoaded;
		}

		private void PageBaseLoaded( object sender, System.Windows.RoutedEventArgs e )
		{
			var viewModel = DataContext as ViewModelBase;
			if( viewModel != null )
			{
				viewModel.NavigationService = NavigationService;
			}
		}

		protected override void OnNavigatedTo( System.Windows.Navigation.NavigationEventArgs e )
		{
			base.OnNavigatedTo( e );			
			var viewModel = DataContext as ViewModelBase;
			if( viewModel != null )
			{
				viewModel.NavigationContext = NavigationContext;
				viewModel.OnNavigatedTo( e );
			}
		}

		protected override void OnNavigatingFrom( System.Windows.Navigation.NavigatingCancelEventArgs e )
		{
			base.OnNavigatingFrom( e );
			var viewModel = DataContext as ViewModelBase;
			if( viewModel != null )
			{
				viewModel.NavigationContext = NavigationContext;
				viewModel.OnNavigatingFrom( e );
			}
		}

		protected override void OnNavigatedFrom( System.Windows.Navigation.NavigationEventArgs e )
		{
			base.OnNavigatedFrom( e );
			var viewModel = DataContext as ViewModelBase;
			if( viewModel != null )
			{
				viewModel.NavigationContext = NavigationContext;
				viewModel.OnNavigatedFrom( e );
			}
		}

    }
}
