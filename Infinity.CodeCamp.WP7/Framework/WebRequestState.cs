﻿using System.Collections.Generic;
using System.Net;

namespace Infinity.CodeCamp.WP7.Framework
{
    public class WebRequestState
    {
        public HttpWebRequest Request { get; set; }

        public Dictionary<string, object> UserData { get; set; }

        public WebRequestState()
        {
            this.UserData = new Dictionary<string, object>();
        }
    }
}
