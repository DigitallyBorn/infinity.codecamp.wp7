﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Phone.Controls;
using System.Windows.Navigation;

namespace Infinity.CodeCamp.WP7.Framework
{
    public abstract class ViewModelBase : PropertyChangedBase
    {
        protected bool RemoveBackEntry { get; set; }
        public NavigationService NavigationService { get; set; }
        public NavigationContext NavigationContext { get; set; }

        public virtual void OnNavigatedTo(NavigationEventArgs e) { }

        public virtual void OnNavigatingFrom(NavigatingCancelEventArgs e) { }

        public virtual void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (RemoveBackEntry)
            {
                RemoveBackEntry = false;
                NavigationService.RemoveBackEntry();
            }
        }
    }
}
