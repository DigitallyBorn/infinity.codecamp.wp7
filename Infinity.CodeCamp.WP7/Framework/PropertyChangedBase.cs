﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows;

namespace Infinity.CodeCamp.WP7.Framework
{
    public abstract class PropertyChangedBase : DependencyObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public virtual void NotifyOfPropertyChange(string propertyName)
        {
            Deployment.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                var handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }), null);
        }

        public virtual void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property)
        {
            NotifyOfPropertyChange(property.GetMemberInfo().Name);
        }
    }
}
