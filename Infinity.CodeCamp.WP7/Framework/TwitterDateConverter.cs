﻿using System;
using Newtonsoft.Json.Converters;
using System.Globalization;
using Newtonsoft.Json;

namespace Infinity.CodeCamp.WP7.Framework
{
    public class TwitterDateConverter : DateTimeConverterBase
    {
        private static readonly CultureInfo culture = CultureInfo.InvariantCulture;

        /// <summary>
        /// The date pattern for most dates returned by the API
        /// </summary>
        protected const string DateFormat = "ddd MMM dd HH:mm:ss zz00 yyyy";

        /// <summary>
        /// Reads the json.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="serializer">The serializer.</param>
        /// <returns>The parsed value as a DateTime, or null.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null || reader.Value.GetType() != typeof(string))
                return new DateTime();

            DateTime parsedDate;

            return DateTime.TryParseExact(
                (string)reader.Value,
                DateFormat,
                culture,
                DateTimeStyles.None,
                out parsedDate) ? parsedDate : new DateTime();
        }

        /// <summary>
        /// Writes the json.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value.GetType() != typeof(DateTime))
                throw new ArgumentOutOfRangeException("value", "The value provided was not the expected data type.");

            writer.WriteValue(((DateTime)value).ToString(DateFormat, CultureInfo.InvariantCulture));
        }
    }
}
