﻿using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.IO;
using Infinity.CodeCamp.WP7.Framework;
using System;
using System.Collections.Generic;

namespace Infinity.CodeCamp.WP7
{
    public static class ExtensionMethods
    {
        public static MemberInfo GetMemberInfo(this Expression expression)
        {
            var lambda = (LambdaExpression)expression;

            MemberExpression memberExpression;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = (UnaryExpression)lambda.Body;
                memberExpression = (MemberExpression)unaryExpression.Operand;
            }
            else
            {
                memberExpression = (MemberExpression)lambda.Body;
            }

            return memberExpression.Member;
        }

        public static string ReadString(this Stream stream)
        {
            byte[] data = new byte[32768];

            byte[] buffer = new byte[32768];
            using (MemoryStream ms = new MemoryStream())
            {
                bool exit = false;
                while (!exit)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                    {
                        data = ms.ToArray();
                        exit = true;
                    }
                    else
                    {
                        ms.Write(buffer, 0, read);
                    }
                }
            }

            return System.Text.Encoding.UTF8.GetString(data, 0, data.Length);
        }

        public static Dictionary<string, string> QuerystringToDictionary(this Uri uri)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();

            foreach (var item in uri.Query.Split('&'))
            {
                string[] parts = item.Split('=');

                results.Add(parts[0], parts.Length > 1 ? parts[1] : string.Empty);
            }

            return results;
        }
    }
}
