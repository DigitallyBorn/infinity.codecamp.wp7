﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Navigation;
using Infinity.CodeCamp.WP7.Framework;
using Infinity.CodeCamp.WP7.Models;
using System.ComponentModel;
using System.Windows.Input;
using Microsoft.Phone.Controls;
using System.Diagnostics;

namespace Infinity.CodeCamp.WP7.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private DateTime lastFetch;
        private readonly NavigationService navigationService;

        private ICommand selectTweetCommand;
        public ICommand SelectTweetCommand
        {
            get { return this.selectTweetCommand; }
        }

        #region Not great way
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string pageName;
        public string PageName
        {
            get { return pageName; }
            set {
                this.pageName = value;
                this.RaisePropertyChanged("PageName");
            }
        }
        #endregion

        #region Way better properties
        private ObservableCollection<Tweet> tweets;
        public ObservableCollection<Tweet> Tweets
        {
            get { return this.tweets; }
            set
            {
                this.tweets = value;
                this.NotifyOfPropertyChange(() => this.Tweets);
            }
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { 
                isBusy = value;
                this.NotifyOfPropertyChange(() => this.IsBusy);
            }
        }
        
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.DependencyObject"/> class.
        /// </summary>
        /// <remarks></remarks>
        public MainPageViewModel()
        {
            this.PageName = "public";

            this.selectTweetCommand = new DelegateCommand(this.SelectTweetAction);
        }

        public void PublicTimelineCallback(Exception exception, ObservableCollection<Tweet> tweets)
        {
            if (exception != null)
            {
                // Handle the exception.
                MessageBox.Show(string.Format("{0}\n{1}", exception.Message, exception.StackTrace), "Uh oh!", MessageBoxButton.OK);

                return;
            }

            this.Tweets = tweets;
            this.lastFetch = DateTime.Now;
            this.IsBusy = false;
        }

        public override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (lastFetch == null || DateTime.Now.Subtract(new TimeSpan(0, 5, 0)) > lastFetch)
            {
                this.IsBusy = true;
                Tweet.GetPublicTimeline(this.PublicTimelineCallback);
            }
        }

        public void SelectTweetAction(object param)
        {
            Tweet tweet = (Tweet)param;

            PhoneApplicationFrame RootVisual = Application.Current.RootVisual as PhoneApplicationFrame;
            Debug.Assert(RootVisual != null, "Root is null");
            RootVisual.Navigate(new Uri(string.Format("/Views/UserView.xaml?id={0}", tweet.User.id), UriKind.Relative));
        }

        internal void Refresh()
        {
            this.IsBusy = true;
            Tweet.GetPublicTimeline(this.PublicTimelineCallback);
        }
    }
}
