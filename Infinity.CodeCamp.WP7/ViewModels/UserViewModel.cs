﻿using System;
using System.Windows;
using System.Windows.Input;
using Infinity.CodeCamp.WP7.Framework;
using Infinity.CodeCamp.WP7.Models;
using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;

namespace Infinity.CodeCamp.WP7.ViewModels
{
    public class UserViewModel : ViewModelBase
    {
        private long userId;
        private ICommand viewUserWebsiteCommand;

        /// <summary>
        /// Gets the view user website command.
        /// </summary>
        /// <remarks></remarks>
        public ICommand ViewUserWebsiteCommand
        {
            get { return this.viewUserWebsiteCommand; }
        }

        private User user;
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The user.</value>
        /// <remarks></remarks>
        public User User
        {
            get { return user; }
            set
            {
                this.user = value;
                this.NotifyOfPropertyChange(() => this.User);
                this.NotifyOfPropertyChange(() => WebsiteLinkVisibility);
            }
        }

        private ObservableCollection<Tweet> tweets;
        /// <summary>
        /// Gets or sets the tweets.
        /// </summary>
        /// <value>The tweets.</value>
        /// <remarks></remarks>
        public ObservableCollection<Tweet> Tweets
        {
            get { return tweets; }
            set { 
                tweets = value;
                this.NotifyOfPropertyChange(() => this.Tweets);
            }
        }

        /// <summary>
        /// Gets the website link visibility.
        /// </summary>
        /// <remarks></remarks>
        public Visibility WebsiteLinkVisibility
        {
            get
            {
                if (this.user == null || string.IsNullOrEmpty(this.user.WebsiteUrl))
                    return Visibility.Collapsed;

                return Visibility.Visible;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is busy.
        /// </summary>
        /// <remarks></remarks>
        public bool IsBusy
        {
            get { return busyTasks > 0; }
        }

        private int busyTasks;
        /// <summary>
        /// Gets or sets the busy tasks.
        /// </summary>
        /// <value>The busy tasks.</value>
        /// <remarks></remarks>
        public int BusyTasks
        {
            get { return busyTasks; }
            set { 
                busyTasks = value;
                this.NotifyOfPropertyChange(() => this.IsBusy);
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="UserViewModel"/> class.
        /// </summary>
        /// <remarks></remarks>
        public UserViewModel()
        {
            this.viewUserWebsiteCommand = new DelegateCommand(p => ViewWebsite(new Uri(this.user.WebsiteUrl)));
        }

        /// <summary>
        /// Raises the <see cref="E:NavigatedTo"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.Windows.Navigation.NavigationEventArgs"/> instance containing the event data.</param>
        /// <remarks></remarks>
        public override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (!long.TryParse(this.NavigationContext.QueryString["id"], out this.userId))
            {
                this.NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                return;
            }

            this.BusyTasks++;
            User.GetUserDetails(
                userId, 
                (ex, user) =>
                {
                    if (ex != null)
                    {
                        MessageBox.Show(string.Format("{0}\n{1}", ex.Message, ex.StackTrace), "Uh oh!", MessageBoxButton.OK);
                    }

                    this.User = user;
                    this.BusyTasks--;
                });

            this.BusyTasks++;
            Tweet.GetUserTimeline(userId,
                (ex, tweets) =>
                {
                    if (ex != null)
                    {
                        MessageBox.Show(string.Format("{0}\n{1}", ex.Message, ex.StackTrace), "Uh oh!", MessageBoxButton.OK);
                    }

                    this.Tweets = tweets;
                    this.BusyTasks--;
                });
        }

        /// <summary>
        /// Views the user on twitter.
        /// </summary>
        /// <remarks></remarks>
        public void ViewUserOnTwitter()
        {
            if (this.user == null)
                return;

            ViewWebsite(this.user.ProfileUri);
        }

        public void ViewUserWebsite()
        {
            if (this.user == null || string.IsNullOrEmpty(this.user.WebsiteUrl))
                return;

            ViewWebsite(new Uri(this.user.WebsiteUrl));
        }

        /// <summary>
        /// Views the website.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <remarks></remarks>
        private void ViewWebsite(Uri uri)
        {
            WebBrowserTask task = new WebBrowserTask();
            task.Uri = uri;
            task.Show();
        }
    }
}
