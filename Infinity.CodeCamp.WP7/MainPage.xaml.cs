﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Infinity.CodeCamp.WP7.ViewModels;
using Infinity.CodeCamp.WP7.Framework;

namespace Infinity.CodeCamp.WP7
{
    public partial class MainPage : ViewPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            this.DataContext = new MainPageViewModel();
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            ((MainPageViewModel)this.DataContext).Refresh();
        }
    }
}