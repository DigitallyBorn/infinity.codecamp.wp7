The Infinity.CodeCamp.WP7 project is a demonstration Twitter application for Windows Phone Phone 7.1.

To work with this project, you will need:
* Visual Studio 2010 SP1

* Windows Phone 7.1 SDK
	http://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=27570

OR

* Visual Studio 2010 Express for WP7 
	http://www.microsoft.com/visualstudio/en-us/products/2010-editions/windows-phone-developer-tools


Here are some other useful resources for developing WP7 applications:
* Silverlight for WP7 toolkit
	http://silverlight.codeplex.com/releases/view/75888

* Coding4Fun toolkit for WP7
	http://coding4fun.codeplex.com/

* Caliburn.Micro (a really nice MVVM framework)
	http://caliburnmicro.codeplex.com/